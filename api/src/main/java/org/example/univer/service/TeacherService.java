package org.example.univer.service;

import org.example.univer.dto.StudentDto;
import org.example.univer.dto.TeacherDto;

import java.util.List;

public interface TeacherService {
    TeacherDto addTeacher(TeacherDto teacherDto);

    TeacherDto updateTeacher(TeacherDto teacherDto);

    void deleteTeacher(Long id);

    TeacherDto findById(Long id);

    List<TeacherDto> findAll();

    List<StudentDto> getStudents(Long id);
}
