package org.example.univer.service;

import org.example.univer.dto.StudentDto;
import org.example.univer.dto.TeacherDto;

import java.util.List;

public interface StudentsService {
    StudentDto addStudent(StudentDto studentDto);

    StudentDto updateStudent(StudentDto studentDto);

    void deleteStudent(Long id);

    StudentDto findById(Long id);

    List<StudentDto> findAll();

    List<TeacherDto> getTeachers(Long id);
}
