package org.example.univer.service;

public interface TeacherStudentsService {
    void add(Long teacher, Long student);

    void delete(Long teacher, Long student);
}
