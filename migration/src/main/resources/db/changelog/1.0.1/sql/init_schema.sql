create sequence if not exists students_id_seq;
create sequence if not exists teachers_id_seq;
create sequence if not exists chairs_id_seq;
create sequence if not exists specialities_id_seq;

create table if not exists students
(
    id         bigint      not null unique default nextval('students_id_seq'),
    first_name varchar(50) not null,
    last_name  varchar(50) not null,
    patronymic varchar(50) not null,
    spec_id    bigint      not null,
    grade      int,
    constraint students_pk
    primary key (id)
    );

create table if not exists teachers
(
    id         bigint      not null unique default nextval('teachers_id_seq'),
    first_name varchar(50) not null,
    last_name  varchar(50) not null,
    patronymic varchar(50) not null,
    chair_id   bigint      not null,
    constraint teachers_pk
    primary key (id)
    );

create table if not exists chairs
(
    id         bigint       not null unique default nextval('chairs_id_seq'),
    chair_name varchar(100) not null,
    constraint chair_pk
    primary key (id)
    );

create table if not exists specialities
(
    id        bigint       not null unique default nextval('specialities_id_seq'),
    spec_name varchar(200) not null,
    constraint spec_pk
    primary key (id)
    );

create table if not exists teachers_students
(
    student_id bigint,
    teacher_id bigint,
    constraint teachers_students_pk
    primary key (student_id, teacher_id),
    constraint student_fk
    foreign key (student_id)
    references students (id),
    constraint teachers_fk
    foreign key (teacher_id)
    references teachers (id)
    );

alter sequence students_id_seq
    owned by students.id;
alter sequence teachers_id_seq
    owned by teachers.id;
alter sequence chairs_id_seq
    owned by chairs.id;
alter sequence specialities_id_seq
    owned by specialities.id;

alter table students
    add constraint spec_fk
        foreign key (spec_id)
            references specialities (id);

alter table teachers
    add constraint chair_fk
        foreign key (chair_id)
            references chairs (id);