drop table if exists teachers_students;
drop table if exists teachers;
drop table if exists students;
drop table if exists chairs;
drop table if exists specialities;

drop sequence if exists specialities_id_seq;
drop sequence if exists chairs_id_seq;
drop sequence if exists teachers_id_seq;
drop sequence if exists students_id_seq;