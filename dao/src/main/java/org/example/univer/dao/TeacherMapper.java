package org.example.univer.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectKey;
import org.example.univer.entity.StudentEntity;
import org.example.univer.entity.TeacherEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface TeacherMapper {
    @Insert("insert into teachers(id, first_name, last_name, patronymic, chair_id) values " +
            "(#{id}, #{firstName}, #{lastName}, #{patronymic}, #{chair.id})")
    @SelectKey(keyProperty = "id", before = true, resultType = Long.class,
            statement = "select nextval('teachers_id_seq')")
    void insert(TeacherEntity teacherEntity);

    void update(TeacherEntity teacherEntity);

    Optional<TeacherEntity> findById(Long id);

    List<TeacherEntity> findAll();

    void delete(Long id);

    List<StudentEntity> getStudents(Long id);
}
