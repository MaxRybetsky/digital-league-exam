package org.example.univer.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectKey;
import org.example.univer.entity.StudentEntity;
import org.example.univer.entity.TeacherEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface StudentMapper {
    @Insert("insert into students(id, first_name, last_name, patronymic, spec_id, grade) values " +
            "(#{id}, #{firstName}, #{lastName}, #{patronymic}, #{speciality.id}, #{grade})")
    @SelectKey(keyProperty = "id", before = true, resultType = Long.class,
            statement = "select nextval('students_id_seq')")
    void insert(StudentEntity studentEntity);

    void update(StudentEntity studentEntity);

    Optional<StudentEntity> findById(Long id);

    List<StudentEntity> findAll();

    void delete(Long id);

    List<TeacherEntity> getTeachers(Long id);
}
