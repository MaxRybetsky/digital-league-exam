package org.example.univer.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectKey;
import org.example.univer.entity.StudentEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface TeachersStudentsMapper {
    void add(@Param("teacher") Long teacherId, @Param("student") Long studentId);

    void delete(@Param("teacher") Long teacherId, @Param("student") Long studentId);
}
