package org.example.univer.dto;

import lombok.Data;

@Data
public class StudentDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private SpecialityDto speciality;
    private Integer grade;
}
