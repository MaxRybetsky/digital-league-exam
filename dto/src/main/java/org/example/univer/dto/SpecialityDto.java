package org.example.univer.dto;

import lombok.Data;

@Data
public class SpecialityDto {
    private Long id;
    private String name;
}
