package org.example.univer.dto;

import lombok.Data;

@Data
public class ChairDto {
    private Long id;
    private String name;
}
