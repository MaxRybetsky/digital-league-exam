package org.example.univer.service;

import lombok.RequiredArgsConstructor;
import org.example.univer.dao.TeachersStudentsMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeacherStudentsServiceImpl implements TeacherStudentsService {
    private final TeachersStudentsMapper teachersStudentsMapper;

    @Override
    public void add(Long teacher, Long student) {
        teachersStudentsMapper.add(teacher, student);
    }

    @Override
    public void delete(Long teacher, Long student) {
        teachersStudentsMapper.delete(teacher, student);
    }
}
