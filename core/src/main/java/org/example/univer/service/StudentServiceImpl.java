package org.example.univer.service;

import lombok.RequiredArgsConstructor;
import org.example.univer.dao.StudentMapper;
import org.example.univer.dto.StudentDto;
import org.example.univer.dto.TeacherDto;
import org.example.univer.entity.StudentEntity;
import org.example.univer.entity.TeacherEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentsService {
    private final StudentMapper studentMapper;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public StudentDto addStudent(StudentDto studentDto) {
        StudentEntity studentEntity = modelMapper.map(studentDto, StudentEntity.class);
        studentMapper.insert(studentEntity);
        return findById(studentEntity.getId());
    }

    @Override
    @Transactional
    public StudentDto updateStudent(StudentDto studentDto) {
        StudentEntity studentEntity = modelMapper.map(studentDto, StudentEntity.class);
        studentMapper.update(studentEntity);
        return findById(studentEntity.getId());
    }

    @Override
    @Transactional
    public void deleteStudent(Long id) {
        studentMapper.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public StudentDto findById(Long id) {
        StudentEntity studentEntity = studentMapper.findById(id).orElseThrow(
                () -> new RuntimeException("No student with ID = " + id)
        );
        return modelMapper.map(studentEntity, StudentDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudentDto> findAll() {
        List<StudentEntity> studentEntities = studentMapper.findAll();
        return modelMapper.map(
                studentEntities,
                new TypeToken<List<StudentDto>>() {
                }.getType()
        );
    }

    @Override
    public List<TeacherDto> getTeachers(Long id) {
        List<TeacherEntity> teacherEntities = studentMapper.getTeachers(id);
        return modelMapper.map(
                teacherEntities,
                new TypeToken<List<TeacherDto>>() {
                }.getType()
        );
    }
}
