package org.example.univer.service;

import lombok.RequiredArgsConstructor;
import org.example.univer.dao.TeacherMapper;
import org.example.univer.dto.StudentDto;
import org.example.univer.dto.TeacherDto;
import org.example.univer.entity.StudentEntity;
import org.example.univer.entity.TeacherEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {
    private final TeacherMapper teacherMapper;
    private final ModelMapper modelMapper;

    @Override
    public TeacherDto addTeacher(TeacherDto teacherDto) {
        TeacherEntity teacherEntity = modelMapper.map(teacherDto, TeacherEntity.class);
        teacherMapper.insert(teacherEntity);
        return findById(teacherEntity.getId());
    }

    @Override
    public TeacherDto updateTeacher(TeacherDto teacherDto) {
        TeacherEntity teacherEntity = modelMapper.map(teacherDto, TeacherEntity.class);
        teacherMapper.update(teacherEntity);
        return findById(teacherEntity.getId());
    }

    @Override
    public void deleteTeacher(Long id) {
        teacherMapper.delete(id);
    }

    @Override
    public TeacherDto findById(Long id) {
        TeacherEntity teacherEntity = teacherMapper.findById(id).orElseThrow(
                () -> new RuntimeException("No teacher with ID = " + id)
        );
        return modelMapper.map(teacherEntity, TeacherDto.class);
    }


    @Override
    public List<TeacherDto> findAll() {
        List<TeacherEntity> teacherEntities = teacherMapper.findAll();
        return modelMapper.map(
                teacherEntities,
                new TypeToken<List<TeacherDto>>() {
                }.getType()
        );
    }

    @Override
    public List<StudentDto> getStudents(Long id) {
        List<StudentEntity> studentEntities = teacherMapper.getStudents(id);
        return modelMapper.map(
                studentEntities,
                new TypeToken<List<StudentDto>>() {
                }.getType()
        );
    }
}
