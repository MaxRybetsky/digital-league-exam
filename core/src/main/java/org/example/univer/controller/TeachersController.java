package org.example.univer.controller;

import com.zaxxer.hikari.util.ConcurrentBag;
import lombok.RequiredArgsConstructor;
import org.example.univer.dto.StudentDto;
import org.example.univer.dto.TeacherDto;
import org.example.univer.service.TeacherService;
import org.example.univer.service.TeacherStudentsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/teachers")
@RequiredArgsConstructor
public class TeachersController {
    private final TeacherService teacherService;
    private final TeacherStudentsService teacherStudentsService;

    @PostMapping
    public TeacherDto addTeacher(@RequestBody TeacherDto teacherDto) {
        return teacherService.addTeacher(teacherDto);
    }

    @GetMapping("/{id}")
    public TeacherDto getTeacher(@PathVariable Long id) {
        return teacherService.findById(id);
    }

    @GetMapping
    public List<TeacherDto> getAll() {
        return teacherService.findAll();
    }

    @PutMapping
    public TeacherDto updateTeacher(@RequestBody TeacherDto teacherDto) {
        return teacherService.updateTeacher(teacherDto);
    }

    @DeleteMapping("/{id}")
    public void deleteTeacher(@PathVariable Long id) {
        teacherService.deleteTeacher(id);
    }

    @GetMapping("{id}/students")
    public List<StudentDto> getStudents(@PathVariable Long id) {
        return teacherService.getStudents(id);
    }

    @PostMapping("/{teacher}/students/{student}}")
    public String add(@PathVariable Long student,
                      @PathVariable Long teacher) {
        teacherStudentsService.add(teacher, student);
        return "Added";
    }

    @DeleteMapping("/{teacher}/students/{student}}")
    public String delete(@PathVariable Long student,
                      @PathVariable Long teacher) {
        teacherStudentsService.delete(teacher, student);
        return "Deleted";
    }
}
