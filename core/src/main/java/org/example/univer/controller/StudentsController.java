package org.example.univer.controller;

import lombok.RequiredArgsConstructor;
import org.example.univer.dto.StudentDto;
import org.example.univer.dto.TeacherDto;
import org.example.univer.service.StudentsService;
import org.example.univer.service.TeacherStudentsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
@RequiredArgsConstructor
public class StudentsController {
    private final StudentsService studentsService;
    private final TeacherStudentsService teacherStudentsService;

    @PostMapping
    public StudentDto addStudent(@RequestBody StudentDto studentDto) {
        return studentsService.addStudent(studentDto);
    }

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id) {
        return studentsService.findById(id);
    }

    @GetMapping
    public List<StudentDto> getAll() {
        return studentsService.findAll();
    }

    @PutMapping
    public StudentDto updateStudent(@RequestBody StudentDto studentDto) {
        return studentsService.updateStudent(studentDto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id) {
        studentsService.deleteStudent(id);
    }

    @GetMapping("{id}/teachers")
    public List<TeacherDto> getTeachers(@PathVariable Long id) {
        return studentsService.getTeachers(id);
    }

    @PostMapping("/{student}/teachers/{teacher}")
    public String add(@PathVariable Long student,
                      @PathVariable Long teacher) {
        teacherStudentsService.add(teacher, student);
        return "Added";
    }

    @DeleteMapping("/{student}/teachers/{teacher}")
    public String delete(@PathVariable Long student,
                      @PathVariable Long teacher) {
        teacherStudentsService.delete(teacher, student);
        return "Deleted";
    }
}
