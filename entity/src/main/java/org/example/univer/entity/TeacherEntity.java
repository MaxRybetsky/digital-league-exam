package org.example.univer.entity;

import lombok.Data;

@Data
public class TeacherEntity {
    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private ChairEntity chair;
}
