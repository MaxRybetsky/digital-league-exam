package org.example.univer.entity;

import lombok.Data;

@Data
public class ChairEntity {
    private Long id;
    private String name;
}
