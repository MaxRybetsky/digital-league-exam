package org.example.univer.entity;

import lombok.Data;

@Data
public class SpecialityEntity {
    private Long id;
    private String name;
}
