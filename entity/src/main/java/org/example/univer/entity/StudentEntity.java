package org.example.univer.entity;

import lombok.Data;

@Data
public class StudentEntity {
    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private SpecialityEntity speciality;
    private Integer grade;
}
